import actionTypes from './actionTypes';

const { FETCH_COUNTRIES } = actionTypes;

const fetchCountriesAC = ({ data, searchData }) => {
	return {
		type: FETCH_COUNTRIES,
		data,
		searchData: searchData,
	};
};

export default fetchCountriesAC;
