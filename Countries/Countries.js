import React, { useState, useEffect } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import { View, StyleSheet } from 'react-native';
import SVGImage from '../SVGRenderer';
import { withTheme, Searchbar, List, Card, Text } from 'react-native-paper';
import { Header } from './components';
import { fetchCountriesAC } from '../Actions';
import Country from '../Country';

const changeSearchText = ({ value, updateSearchText, searchCountries, data }) => {
	updateSearchText(value);
	const filteredData = data.filter(ele => ele.name.toLowerCase().includes(value.toLowerCase()));
	searchCountries({ filteredData, data });
};

const handleRegionPress = ({ searchCountries, data, value, setFiltersMenu, showFiltersMenu }) => {
	const filteredData = data.filter(ele => ele.region.toLowerCase().includes(value.toLowerCase()));
	searchCountries({ filteredData, data });
	setFiltersMenu(!showFiltersMenu);
};

const toggleFiltersMenu = ({ showFiltersMenu, setFiltersMenu }) => {
	setFiltersMenu(!showFiltersMenu);
};

const handleDetailPress = ({ setDetailView, searchData, id, filterCountryData }) => {
	const data = searchData.filter(data => data.alpha3Code === id);
	filterCountryData(data[0]);
	setDetailView(true);
};

const continents = [`Africa`, `America`, `Asia`, `Europe`, `Oceania`];

const Countries = props => {
	const {
		theme: { lightTheme, darkTheme },
		fetchCountries,
		searchData,
		searchCountries,
		data,
	} = props;

	const [themeType, setTheme] = useState(`dark`);
	const [searchText, updateSearchText] = useState(``);
	const [showFiltersMenu, setFiltersMenu] = useState(false);
	const [detailView, setDetailView] = useState(false);
	const [countryData, filterCountryData] = useState({});

	const { colors } = themeType === 'dark' ? darkTheme : lightTheme;

	useEffect(() => {
		fetchCountries({ useFilter: false });
	}, []);

	return (
		<View style={[styles.countriesContainer, { backgroundColor: colors.darkBackground }]}>
			<Header themeType={themeType} setTheme={setTheme} />
			{detailView ? (
				<Country
					data={countryData}
					filterCountryData={filterCountryData}
					setDetailView={setDetailView}
					themeType={themeType}
					countriesData={data}
				/>
			) : (
				<View>
					<Searchbar
						style={[
							{ backgroundColor: colors.lightBackground, color: colors.fontColor },
							styles.searchContainer,
						]}
						inputStyle={{ color: colors.fontColor }}
						placeholderTextColor={colors.fontColor}
						iconColor={colors.fontColor}
						placeholder="Search for a country..."
						value={searchText}
						onChangeText={value => changeSearchText({ value, updateSearchText, searchCountries, data })}
					/>
					<List.Section>
						<List.Accordion
							title="Filter by Region"
							expanded={showFiltersMenu}
							onPress={() => toggleFiltersMenu({ showFiltersMenu, setFiltersMenu })}
							style={[{ backgroundColor: colors.lightBackground }, styles.listStyle]}
							titleStyle={{ color: colors.fontColor }}
						>
							{continents.map((continent, key) => (
								<List.Item
									title={continent}
									key={key}
									style={[
										styles.listStyle,
										{
											backgroundColor: colors.lightBackground,
											borderRadius: 0,
										},
										continent === 'Africa' && {
											borderTopLeftRadius: 4,
											borderTopRightRadius: 4,
											marginTop: 4,
										},
										continent === 'Oceania' && {
											borderBottomLeftRadius: 4,
											borderBottomRightRadius: 4,
										},
									]}
									titleStyle={{ color: colors.fontColor }}
									onPress={() =>
										handleRegionPress({
											searchCountries,
											data,
											value: continent,
											setFiltersMenu,
											showFiltersMenu,
										})
									}
								/>
							))}
						</List.Accordion>
					</List.Section>
					{searchData.length > 0 ? (
						searchData.map(ele => (
							<View style={styles.cardContainer} key={ele.alpha3Code}>
								<Card
									style={[{ backgroundColor: colors.lightBackground, color: colors.fontColor }]}
									onPress={() =>
										handleDetailPress({
											setDetailView,
											searchData,
											id: ele.alpha3Code,
											filterCountryData,
										})
									}
								>
									<SVGImage style={{ width: `100%`, height: 200 }} source={{ uri: ele.flag }} />
									<Text style={[{ color: colors.fontColor }, styles.cardTitleText]}>{ele.name}</Text>
									<View style={styles.descriptionContainer}>
										<View style={styles.flexRow}>
											<Text
												style={[{ color: colors.fontColor }, styles.boldFont]}
											>{`Population: `}</Text>
											<Text style={{ color: colors.fontColor }}>{ele.population}</Text>
										</View>
										<View style={styles.flexRow}>
											<Text
												style={[{ color: colors.fontColor }, styles.boldFont]}
											>{`Region: `}</Text>
											<Text style={{ color: colors.fontColor }}>{ele.region}</Text>
										</View>
										<View style={styles.flexRow}>
											<Text
												style={[{ color: colors.fontColor }, styles.boldFont]}
											>{`Capital: `}</Text>
											<Text style={{ color: colors.fontColor }}>{ele.capital}</Text>
										</View>
									</View>
								</Card>
							</View>
						))
					) : (
						<Text style={[{ color: colors.fontColor }, styles.searchEmptyText]}>
							{`Country Not found matching your search term. Try Different keywords.`}
						</Text>
					)}
				</View>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	countriesContainer: {
		flex: 1,
	},
	searchContainer: {
		marginLeft: 12,
		marginRight: 12,
		marginTop: 30,
		marginBottom: 30,
	},
	listStyle: {
		marginLeft: 12,
		marginRight: 12,
		width: `55%`,
		borderRadius: 4,
		elevation: 5,
	},
	cardContainer: {
		marginLeft: 36,
		marginRight: 36,
		marginTop: 30,
	},
	flexRow: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	boldFont: {
		fontWeight: `bold`,
		marginRight: 8,
		fontSize: 18,
	},
	descriptionContainer: {
		paddingLeft: 20,
		paddingRight: 20,
		paddingBottom: 20,
		paddingTop: 16,
	},
	cardTitleText: {
		fontSize: 20,
		fontWeight: `bold`,
		paddingLeft: 20,
		paddingTop: 12,
	},
	searchEmptyText: {
		marginLeft: 12,
		marginRight: 12,
		marginTop: 30,
		marginBottom: 30,
	},
});

const mapStateToProps = state => {
	const {
		countries: { searchData, data },
	} = state;
	return { searchData, data };
};

const mapDispatchToProps = dispatch => {
	return {
		fetchCountries: async () => {
			const APIEndPoint = `https://restcountries.eu/rest/v2/all`;
			try {
				const response = await axios.get(APIEndPoint);
				dispatch(fetchCountriesAC({ data: response.data, searchData: response.data }));
			} catch (error) {
				console.error(error);
			}
		},
		searchCountries: ({ data, filteredData }) => {
			dispatch(fetchCountriesAC({ data, searchData: filteredData }));
		},
	};
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withTheme)(Countries);
