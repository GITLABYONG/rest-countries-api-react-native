const colors = {};
colors.darkBlue = `hsl(209, 23%, 22%)`;
colors.veryDarkBlueBG = `hsl(207, 26%, 17%)`;
colors.veryDarkBlue = `hsl(200, 15%, 8%)`;
colors.darkGrey = `hsl(0, 0%, 52%)`;
colors.veryLightGrey = `hsl(0, 0%, 98%)`;
colors.white = `hsl(0, 0%, 100%)`;

export default colors;
